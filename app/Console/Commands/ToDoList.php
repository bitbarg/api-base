<?php

namespace App\Console\Commands;

use BankApp\Models\Task;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ToDoList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Artisan Command For To Do List';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tasks=Task::where('time', '>',Carbon::now()->addMinute(30)->toDateTimeString())
            ->where('status','waiting')->get();

        foreach ($tasks as $task){
            echo 'id:'.$task['id'].'-';
            echo 'user_id:'.$task['user_id'].'-';
            echo 'title:'.$task['title'].'-';
            echo 'time:'.$task['time'].'-';
            echo 'description:'.$task['description'].'-';
            echo 'color:'.$task['color'].'-';
        }

        Task::where('time', '>',Carbon::now()->addMinute(30)->toDateTimeString())
            ->where('status','waiting')
            ->update(['status' => 'sending']);

    }
}
