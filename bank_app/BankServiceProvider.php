<?php

namespace BankApp;

use App\Models\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use BankApp\Models\Task;

class BankServiceProvider extends ServiceProvider
{

    public function register()
    {
        User::resolveRelationUsing('tasks', function () {
            return $this->hasMany(Task::class);
        });
    }


    public function boot()
    {

        //Register route api_bank_routes
        Route::prefix('api')
            ->middleware('api')
            ->group(base_path('bank_app/route/api/bank_routes.php'));

        //Register migrations api_bank_routes
        $this->loadMigrationsFrom([
            base_path('bank_app/database/migrations')
        ]);

    }
}
