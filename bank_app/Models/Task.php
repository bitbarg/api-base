<?php

namespace BankApp\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Task extends Model
{
    use HasFactory;

    protected $guarded=[];

    protected $hidden=['user_id','updated_at','created_at'];

    public static function CreateTable()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('title', 30);
            $table->dateTime('time');
            $table->string('description', 500)->nullable();
            $table->string('color', 30)->nullable();
            $table->enum('status', ['waiting', 'sending'])->default('waiting');
            $table->timestamps();
        });
    }

    public static function DropTable()
    {
        Schema::dropIfExists('tasks');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
