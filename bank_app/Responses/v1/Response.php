<?php

namespace BankApp\Responses\v1;

class Response
{
    public static function failed()
    {
        return response()->json(
            [
                "success"=>false,
                "message"=>"Execute Error",
                "apiVersion"=>"v1.1.0",
                "error"=>400,
                "result"=>'Operation Is Failed',
            ]);
    }

    public static function success($data)
    {
        return response()->json(
                [
                    "success"=>true,
                    "message"=>"Execute Successfully",
                    "apiVersion"=>"v1.1.0",
                    "error"=>null,
                    "result"=>$data,
                ]);
    }

    public static function validationFailed()
    {
        return response()->json(
            [
                "success"=>false,
                "message"=>"Validation Error",
                "apiVersion"=>"v1.1.0",
                "error"=>null,
                "result"=>'Please enter data',
            ]);
    }

    public static function permission()
    {
        return 'You do not have permission';
    }

    public static function notFind($id)
    {
        return 'Not find '.$id;
    }



}
