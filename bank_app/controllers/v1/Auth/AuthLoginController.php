<?php

namespace BankApp\controllers\v1\Auth;

use BankApp\protectionLayer\Auth\AuthLogin;
use BankApp\protectionLayer\validationApi;
use BankApp\Responses\v1\Response;
use Imanghafoori\HeyMan\Facades\HeyMan;
use Imanghafoori\HeyMan\StartGuarding;

class AuthLoginController
{

    public function __construct()
    {
        validationApi::install();
        resolve(StartGuarding::class)->start();

    }
    public function login()
    {
        HeyMan::checkPoint('api.login');  //check point the same guard

        event('task.login');

        $data = request()->only(['email', 'password']);

        $user = AuthLogin::login($data)->getOr(Response::failed());

        return Response::success($user);

    }

}
