<?php

namespace BankApp\controllers\v1\Auth;


use BankApp\protectionLayer\Auth\AuthLogOut;
use BankApp\Responses\v1\Response;

class AuthLogoutController
{

    public function logout()
    {
        $user=AuthLogOut::logout()->getOrSend([Response::class,'failed']);

        return Response::success($user);

    }
}
