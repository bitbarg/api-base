<?php

namespace BankApp\controllers\v1\Auth;

use BankApp\protectionLayer\Auth\AuthRegister;
use BankApp\protectionLayer\validationApi;
use BankApp\Responses\v1\Response;
use Imanghafoori\HeyMan\Facades\HeyMan;
use Imanghafoori\HeyMan\StartGuarding;

class AuthRegisterController
{

    public function __construct()
    {
        validationApi::install();
        resolve(StartGuarding::class)->start();
    }

    public function register()
    {
        HeyMan::checkPoint('api.register'); //check point the same guard

        $data = request()->only(['name', 'email', 'password', 'password_confirmation']);

        $user = AuthRegister::register($data)->getOrSend([Response::failed()]);

        return Response::success($user);
    }
}
