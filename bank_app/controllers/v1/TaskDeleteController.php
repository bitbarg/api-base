<?php

namespace BankApp\controllers\v1;

use BankApp\protectionLayer\Task\TaskDelete;
use BankApp\Responses\v1\Response;
class TaskDeleteController
{

    public function delete($id)
    {
        $task = TaskDelete::remove($id,auth()->id())->getOrSend([Response::failed()]);

        return Response::success($task);
    }
}
