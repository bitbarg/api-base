<?php

namespace BankApp\controllers\v1;

use BankApp\protectionLayer\Task\TaskList;
use BankApp\Responses\v1\Response;

class TaskListController
{

    public function list()
    {
        $task = TaskList::read(auth()->id())->getOrSend([Response::failed()]);

        return Response::success($task);

    }
}
