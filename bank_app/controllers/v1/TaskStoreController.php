<?php

namespace BankApp\controllers\v1;

use BankApp\protectionLayer\Task\TaskStore;
use BankApp\protectionLayer\validationApi;
use BankApp\Responses\v1\Response;
use Imanghafoori\HeyMan\Facades\HeyMan;
use Imanghafoori\HeyMan\StartGuarding;

class TaskStoreController
{

    public function __construct()
    {
        validationApi::install();
        resolve(StartGuarding::class)->start();
    }

    public function store()
    {
        HeyMan::checkPoint('tasks.store');//check point the same guard

        $data = request()->only(['title', 'time', 'description', 'color']);

        $task = TaskStore::store($data, auth()->id())->getOrSend([Response::failed()]);

        return Response::success($task);

    }
}
