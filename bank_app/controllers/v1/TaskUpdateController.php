<?php

namespace BankApp\controllers\v1;

use BankApp\protectionLayer\Task\TaskUpdate;
use BankApp\Responses\v1\Response;

class TaskUpdateController
{

    public function update($id)
    {
        $data = request()->only(['title', 'time', 'description', 'color']);
        $task = TaskUpdate::update($id,$data, auth()->id())->getOrSend([Response::failed()]);

        return Response::success($task);

    }
}
