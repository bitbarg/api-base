<?php

use Illuminate\Database\Migrations\Migration;
use BankApp\Models\Task;

return new class extends Migration
{
    public function up()
    {
        Task::CreateTable();
    }

    public function down()
    {
        Task::DropTable();
    }

};
