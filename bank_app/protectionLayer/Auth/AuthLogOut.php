<?php

namespace BankApp\protectionLayer\Auth;

use App\Models\User;
use Imanghafoori\Helpers\Nullable;

class AuthLogOut
{
    public static function logout() : Nullable
    {
        try {

            auth()->user()->tokens()->delete();
            $response = [
                'message' => 'Logged out'
            ];

            return nullable($response);

        }catch (\Exception $e){

            return nullable(null);
        }


    }

}
