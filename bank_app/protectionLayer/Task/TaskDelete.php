<?php

namespace BankApp\protectionLayer\Task;

use BankApp\Models\Task;
use BankApp\Responses\v1\Response;
use Imanghafoori\Helpers\Nullable;

class TaskDelete
{
    public static function remove($Id,$userId): Nullable
    {
        try {

            $item = Task::find($Id);
            if($item != null)
                return  self::queryDelete($item,$userId,$Id);

        } catch (\Exception $exception) {

            return nullable(null);
        }

       return nullable(Response::notFind($Id));
    }

    public static function queryDelete($query,$userId,$Id)
    {
        $result = $query->where('id',$Id)->where('user_id', $userId)->delete();
        if ($result)
            return nullable($query);

        return nullable(Response::permission());



    }
}
