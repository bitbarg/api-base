<?php

namespace BankApp\protectionLayer\Task;

use BankApp\Models\Task;
use Imanghafoori\Helpers\Nullable;

class TaskList
{
    public static function read($userId): Nullable
    {
        try {

//            $tasks = Cache::remember('tasks',3600,function ($userId){
//                return Task::query()->where('user_id',$userId)->orderBy('order')->get();
//            });

            $tasks = Task::query()->where('user_id',$userId)->get();

        } catch (\Exception $exception) {

            return nullable(null);
        }

        if (!$tasks) {
            return nullable(null);
        }

        return nullable($tasks);

    }
}
