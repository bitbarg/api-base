<?php

namespace BankApp\protectionLayer\Task;

use BankApp\Models\Task;
use Imanghafoori\Helpers\Nullable;

class TaskStore
{
    public static function store($data,$userId) : Nullable
    {
        try {

            $task = Task::query()->create($data + ['user_id' => $userId]);

        }catch (\Exception $exception){

            return nullable(null);
        }

        if (! $task->exists) {
            return nullable(null);
        }

        return nullable($task);

    }
}
