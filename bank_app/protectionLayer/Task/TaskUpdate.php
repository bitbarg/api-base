<?php

namespace BankApp\protectionLayer\Task;

use BankApp\Models\Task;
use BankApp\Responses\v1\Response;
use Imanghafoori\Helpers\Nullable;

class TaskUpdate
{
    public static function update($Id,$data,$userId) : Nullable
    {
        try {

            $item = Task::find($Id);
            if($item != null)
                return  self::queryUpdate($item,$userId,$data,$Id);


        }catch (\Exception $exception){

            return nullable(null);
        }


        return nullable(Response::notFind($Id));

    }

    public static function queryUpdate($query,$userId,$data,$Id)
    {
        $result = $query->where('id',$Id)->where('user_id', $userId)->update($data);
        if ($result)
            return nullable(self::lastUpdate($Id));

        return nullable(Response::permission());

    }

    public static function lastUpdate($Id)
    {
        return Task::find($Id);
    }
}
