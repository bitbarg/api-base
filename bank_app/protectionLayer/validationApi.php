<?php

namespace BankApp\protectionLayer;

use BankApp\Responses\v1\Response;
use Imanghafoori\HeyMan\Facades\HeyMan;

class validationApi
{
    public static function install()
    {

        HeyMan::onCheckPoint('api.register')
            ->validate([
                'name'=>'required',
                'email'=>'required|unique:users,email',
                'password'=>'required',
            ])
            ->otherwise()
            ->weRespondFrom([Response::class,'validationFailed']);

        HeyMan::onCheckPoint('api.login')
            ->validate([
                'email'=>'required|email',
                'password'=>'required',
            ])->otherwise()
            ->weRespondFrom([Response::class,'validationFailed']);

        HeyMan::onCheckPoint('tasks.store')
            ->validate([
                'title'=>'required',
                'time'=>'required|date_format:Y-m-d H:i:s',
            ])
            ->otherwise()
            ->weRespondFrom([Response::class,'validationFailed']);

    }


}
