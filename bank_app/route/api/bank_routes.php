<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use BankApp\controllers\v1\Auth\AuthRegisterController;
use BankApp\controllers\v1\Auth\AuthLogoutController;
use BankApp\controllers\v1\Auth\AuthLoginController;
use BankApp\controllers\v1\TaskStoreController;
use BankApp\controllers\v1\TaskListController;
use BankApp\controllers\v1\TaskDeleteController;
use BankApp\controllers\v1\TaskUpdateController;

Route::prefix('v1')->group(function () {

    #region Auth
    Route::post('/register', [AuthRegisterController::class, 'register'])->name('api.register');
    Route::post('/login', [AuthLoginController::class, 'login'])->name('api.login');
    Route::get('/logout', [AuthLogoutController::class, 'logout'])->name('api.logout')->middleware('auth:sanctum');
    #end region

    #region Task
    Route::get('/list', [TaskListController::class, 'list'])->name('api.task.list')->middleware('auth:sanctum');
    Route::post('/create', [TaskStoreController::class, 'store'])->name('api.task.create')->middleware('auth:sanctum');
    Route::post('/edit/{id}', [TaskUpdateController::class, 'update'])->name('api.task.update')->middleware('auth:sanctum');
    Route::delete('/delete/{id}', [TaskDeleteController::class, 'delete'])->name('api.task.delete')->middleware('auth:sanctum');
    #end region





});
